package org.computer.scrapper.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.computer.scrapper.enums.ProductCategory;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@ToString
public class ProductDto {
    private String producer;
    private String name;
    private ProductCategory productCategory;
    private BigDecimal price;

}
