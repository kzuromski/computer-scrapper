package org.computer.scrapper.domain;

import org.computer.scrapper.dto.ProductDto;
import org.computer.scrapper.enums.ProductCategory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class XkomProcessor {

    private RemoteWebDriver webDriver = null;
    public List<ProductDto> processProduct(ProductCategory productCategory) {
        try {
            this.webDriver = new RemoteWebDriver(new URL("http://firefox:4444/wd/hub"), DesiredCapabilities.firefox());
        } catch (MalformedURLException e) {
            e.getMessage();
        }
        String url = "";
        switch (productCategory){
            case GPU:
                url = "https://www.x-kom.pl/g-5/c/346-karty-graficzne-nvidia.html?per_page=90";
                break;
            case CPU:
                url = "https://www.x-kom.pl/g-5/c/11-procesory.html?per_page=90";
                break;
            case MOTHERBOARD:
                url = "https://www.x-kom.pl/g-5/c/14-plyty-glowne.html?per_page=90";
                break;
            case RAM:
                url = "https://www.x-kom.pl/g-5/c/28-pamieci-ram.html?per_page=90";
                break;
            case HARD_DRIVE:
                url = "https://www.x-kom.pl/g-5/c/89-dyski-twarde-hdd-i-ssd.html?per_page=90";
        }
        webDriver.get(url);
        List<WebElement> list = webDriver.findElements(By.cssSelector("div.description-wrapper a.name"));
        List<WebElement> prices = webDriver.findElements(By.cssSelector("span.price"));
        List<ProductDto> productDtos = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            String record = list.get(i).getText();
            if(!record.isEmpty())
            {
                String producer = record.substring(0, record.indexOf(' '));
                String name = record.substring(record.indexOf(' ') + 1);
                String price = prices.get(i).getText();
                price = price.replaceAll(",", ".").replaceAll(" ", "");
                price = price.substring(0, price.length() - 2);
                productDtos.add(new ProductDto(producer, name, productCategory, new BigDecimal(price)));
            }
        }
        this.webDriver.close();
        this.webDriver = null;
        return productDtos;
    }
}
