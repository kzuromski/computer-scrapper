package org.computer.scrapper;
import org.computer.scrapper.enums.ProductCategory;
import org.computer.scrapper.model.Item;
import org.computer.scrapper.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;

@SpringBootApplication
public class ScrapperApplication implements CommandLineRunner{

    public static void main(String[] args) {
        SpringApplication.run(ScrapperApplication.class, args);
    }

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        Item item = new Item();
        item.setName("MSI RTX 2070 GAMING X");
        item.setCategory(ProductCategory.GPU);
        item.setProducer("MSI");
        Product product = new Product();
        product.setPrice(BigDecimal.valueOf(12443));
        item.addProduct(product);
        entityManager.persist(item);

    }
}
