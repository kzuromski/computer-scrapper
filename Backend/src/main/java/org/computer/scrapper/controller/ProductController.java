package org.computer.scrapper.controller;

import org.computer.scrapper.dto.ProductDto;
import org.computer.scrapper.enums.ProductCategory;
import org.computer.scrapper.enums.ProductCategoryConverter;
import org.computer.scrapper.exception.ProductNotFoundException;
import org.computer.scrapper.model.Product;
import org.computer.scrapper.repository.ProductRepository;
import org.computer.scrapper.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    private ProductRepository productRepository;
    private ProductService productService;

    @Autowired
    public ProductController(ProductRepository productRepository, ProductService productService) {
        this.productRepository = productRepository;
        this.productService = productService;
    }

    @GetMapping("/all")
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @GetMapping("/producer/{producer}")
    public List<Product> getProductsByProducer(@PathVariable String producer) {
        //return productRepository.findAllByProducer(producer).orElseThrow(ProductNotFoundException::new);
        return new ArrayList<>();
    }

    @GetMapping("/{name}/details")
    public Product getProductByName(@PathVariable String name) {
        //return productRepository.findByName(name).orElseThrow(ProductNotFoundException::new);
        return new Product();
    }

    @GetMapping("/category/{category}")
    public List<Product> getProductByCategory(@PathVariable ProductCategory category) {
        return productService.getAllProductsByCategory(category);
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody ProductDto product) {
        return productService.save(product);
    }

    @PutMapping("/{id}")
    public Product editProduct(@RequestBody ProductDto product, @PathVariable Long id) {
        Product product1 = productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
        product1.setPrice(product.getPrice());
        return productRepository.save(product1);
    }

    @InitBinder
    public void initBinder(final WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(ProductCategory.class, new ProductCategoryConverter());
    }

}
