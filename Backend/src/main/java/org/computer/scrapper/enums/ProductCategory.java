package org.computer.scrapper.enums;

import java.util.Arrays;

public enum ProductCategory {
    GPU("GPU"),
    CPU("CPU"),
    RAM("RAM"),
    HARD_DRIVE("HardDrive"),
    MOTHERBOARD("MotherBoard");

    private String value;

    ProductCategory(String value) {
        this.value = value;
    }

    public static ProductCategory fromValue(String value) {
        for(ProductCategory category : values()) {
            if(category.value.equalsIgnoreCase(value)){
                return category;
            }
        }
        throw new IllegalArgumentException("Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }

}
