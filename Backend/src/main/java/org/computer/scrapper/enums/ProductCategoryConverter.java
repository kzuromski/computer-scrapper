package org.computer.scrapper.enums;

import java.beans.PropertyEditorSupport;

public class ProductCategoryConverter extends PropertyEditorSupport {
    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(ProductCategory.fromValue(text));
    }
}
