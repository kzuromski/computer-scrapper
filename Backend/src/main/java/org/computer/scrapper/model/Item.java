package org.computer.scrapper.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.computer.scrapper.enums.ProductCategory;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Audited
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String producer;
    private ProductCategory category;
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(joinColumns = @JoinColumn( name="product_id"),
            inverseJoinColumns = @JoinColumn( name="item_id"))
    private Set<Product> products = new HashSet<>();

    public void addProduct(Product product){
        products.add(product);
        product.setItem(this);
    }
    public void removeProduct(Product product){
        products.remove(product);
        product.setItem(null);
    }
}
