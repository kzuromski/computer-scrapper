package org.computer.scrapper.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.computer.scrapper.enums.Shop;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "product")
@Data
@Audited
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @JsonBackReference
    private Item item;
    private BigDecimal price;
    private Shop shop;
    private LocalDate date;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product )) return false;
        return id != null && id.equals(((Product) o).getId());
    }
    @Override
    public int hashCode() {
        return 31;
    }
}
