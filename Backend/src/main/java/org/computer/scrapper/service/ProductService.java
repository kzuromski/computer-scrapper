package org.computer.scrapper.service;

import org.computer.scrapper.dto.ProductDto;
import org.computer.scrapper.enums.ProductCategory;
import org.computer.scrapper.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    Product save(ProductDto productDto);
    List<Product> getAllProductsByCategory(ProductCategory productCategory);
}
