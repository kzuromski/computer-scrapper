package org.computer.scrapper.service;

import org.computer.scrapper.dto.ProductDto;
import org.computer.scrapper.enums.ProductCategory;
import org.computer.scrapper.model.Product;
import org.computer.scrapper.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private ModelMapper modelMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getAllProductsByCategory(ProductCategory productCategory) {
//        return productRepository.findAllByProductCategory(productCategory);
        return new ArrayList<>();
    }

    @Override
    public Product save(ProductDto productDto) {
        Product product = modelMapper.map(productDto, Product.class);
        product.setDate(LocalDate.now());
        return productRepository.save(product);
    }
}
